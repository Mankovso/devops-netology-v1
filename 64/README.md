# Домашнее задание к занятию "6.4. PostgreSQL"

## Задача 1

Используя docker поднимите инстанс PostgreSQL (версию 13). Данные БД сохраните в volume.

Подключитесь к БД PostgreSQL используя `psql`.

Воспользуйтесь командой `\?` для вывода подсказки по имеющимся в `psql` управляющим командам.

**Найдите и приведите** управляющие команды для:
- вывода списка БД
- подключения к БД
- вывода списка таблиц
- вывода описания содержимого таблиц
- выхода из psql

```
Ответ:

docker pull postgres:13
docker volume create vol_postgres
docker run -d -it --name="post-cont" -e POSTGRES_PASSWORD=password -p 5432:5432 -v vol_postgres:/var/lib/postgresql/data postgres:13
docker exec -it post-cont psql -U postgres

postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
(3 rows)

postgres=# \c postgres
You are now connected to database "postgres" as user "postgres".
postgres=# \dt
Did not find any relations.
postgres=# \dtS
                    List of relations
   Schema   |          Name           | Type  |  Owner   
------------+-------------------------+-------+----------
 pg_catalog | pg_aggregate            | table | postgres
 pg_catalog | pg_am                   | table | postgres
 pg_catalog | pg_amop                 | table | postgres

postgres=# \d pg_index
                  Table "pg_catalog.pg_index"
     Column     |     Type     | Collation | Nullable | Default 
----------------+--------------+-----------+----------+---------
 indexrelid     | oid          |           | not null | 
 indrelid       | oid          |           | not null | 
 indnatts       | smallint     |           | not null | 
 indnkeyatts    | smallint     |           | not null | 
 indisunique    | boolean      |           | not null | 
 indisprimary   | boolean      |           | not null | 
 indisexclusion | boolean      |           | not null | 
 indimmediate   | boolean      |           | not null | 
 indisclustered | boolean      |           | not null | 
 indisvalid     | boolean      |           | not null | 
 indcheckxmin   | boolean      |           | not null | 
 indisready     | boolean      |           | not null | 
 indislive      | boolean      |           | not null | 
 indisreplident | boolean      |           | not null | 
 indkey         | int2vector   |           | not null | 
 indcollation   | oidvector    |           | not null | 
 indclass       | oidvector    |           | not null | 
 indoption      | int2vector   |           | not null | 
 indexprs       | pg_node_tree | C         |          | 
 indpred        | pg_node_tree | C         |          | 
Indexes:
    "pg_index_indexrelid_index" UNIQUE, btree (indexrelid)
    "pg_index_indrelid_index" btree (indrelid)

postgres=# \q

```


## Задача 2

Используя `psql` создайте БД `test_database`.

Изучите [бэкап БД](https://github.com/netology-code/virt-homeworks/tree/master/06-db-04-postgresql/test_data).

Восстановите бэкап БД в `test_database`.

Перейдите в управляющую консоль `psql` внутри контейнера.

Подключитесь к восстановленной БД и проведите операцию ANALYZE для сбора статистики по таблице.

Используя таблицу [pg_stats](https://postgrespro.ru/docs/postgresql/12/view-pg-stats), найдите столбец таблицы `orders` 
с наибольшим средним значением размера элементов в байтах.

**Приведите в ответе** команду, которую вы использовали для вычисления и полученный результат.

```
Ответ:

postgres=# CREATE DATABASE test_database;

root@bf5e22bb1cb0:/var/lib/postgresql/data# psql -U postgres -f test_dump.sql test_database

test_database=# ANALYZE VERBOSE public.orders;
INFO:  analyzing "public.orders"
INFO:  "orders": scanned 1 of 1 pages, containing 8 live rows and 0 dead rows; 8 rows in sample, 8 estimated total rows
ANALYZE

test_database=# select avg_width from pg_stats where tablename='orders' ORDER BY avg_width DESC;
 avg_width 
-----------
        16
         4
         4
(3 rows)

```


## Задача 3

Архитектор и администратор БД выяснили, что ваша таблица orders разрослась до невиданных размеров и
поиск по ней занимает долгое время. Вам, как успешному выпускнику курсов DevOps в нетологии предложили
провести разбиение таблицы на 2 (шардировать на orders_1 - price>499 и orders_2 - price<=499).

Предложите SQL-транзакцию для проведения данной операции.

Можно ли было изначально исключить "ручное" разбиение при проектировании таблицы orders?

```
Ответ:

postgres=# \c test_database 
You are now connected to database "test_database" as user "postgres".
test_database=# alter table orders rename to orders_old; (переименовываем таблицу, чтобы в итоге получить новую с темже именем)
ALTER TABLE
test_database=# create table orders (id integer, title varchar(80), price integer) partition by range(price); (сщздаем новую таблицу)
CREATE TABLE
test_database=# create table orders_less499 partition of orders for values from (0) to (499); (до 499)
CREATE TABLE
test_database=# create table orders_more499 partition of orders for values from (499) to (999999999); (после 499)
CREATE TABLE
test_database=# insert into orders (id, title, price) select * from orders_old; (добавляем записи)
INSERT 0 8
test_database=#

При изначальном проектировании таблиц можно было сделать ее секционированной, тогда не пришлось бы переименовывать исходную таблицу и переносить данные в новую.

```

## Задача 4

Используя утилиту `pg_dump` создайте бекап БД `test_database`.

Как бы вы доработали бэкап-файл, чтобы добавить уникальность значения столбца `title` для таблиц `test_database`?

```
Ответ:

oot@bf5e22bb1cb0:/var/lib/postgresql/data# pg_dump -U postgres -d test_database >test_database_dump.sql
root@bf5e22bb1cb0:/var/lib/postgresql/data# dir
base	      pg_hba.conf    pg_notify	   pg_stat	pg_twophase  postgresql.auto.conf  test_database_dump.sql
global	      pg_ident.conf  pg_replslot   pg_stat_tmp	PG_VERSION   postgresql.conf	   test_dump.sql
pg_commit_ts  pg_logical     pg_serial	   pg_subtrans	pg_wal	     postmaster.opts
pg_dynshmem   pg_multixact   pg_snapshots  pg_tblspc	pg_xact      postmaster.pid
root@bf5e22bb1cb0:/var/lib/postgresql/data#

Для уникальности можно добавить индекс или первичный ключ.
    CREATE INDEX ON orders ((lower(title)));

```


---

### Как cдавать задание

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
