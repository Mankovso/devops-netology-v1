# Домашнее задание к занятию "6.6. Troubleshooting"

## Задача 1

Перед выполнением задания ознакомьтесь с документацией по [администрированию MongoDB](https://docs.mongodb.com/manual/administration/).

Пользователь (разработчик) написал в канал поддержки, что у него уже 3 минуты происходит CRUD операция в MongoDB и её 
нужно прервать. 

Вы как инженер поддержки решили произвести данную операцию:
- напишите список операций, которые вы будете производить для остановки запроса пользователя
- предложите вариант решения проблемы с долгими (зависающими) запросами в MongoDB

```
Ответ:

* Вывести все запросы выполняющиеся больше 5 секунд:
use <YOUR-DB>;
db.currentOp().inprog.forEach(
   function(d){
     if(d.secs_running > 5)
        printjson(d)
})

* Надоедливый запрос можно прихлопнуть выполнив:
db.killOp(<opid>)

```


## Задача 2

Перед выполнением задания познакомьтесь с документацией по [Redis latency troobleshooting](https://redis.io/topics/latency).

Вы запустили инстанс Redis для использования совместно с сервисом, который использует механизм TTL. 
Причем отношение количества записанных key-value значений к количеству истёкших значений есть величина постоянная и
увеличивается пропорционально количеству реплик сервиса. 

При масштабировании сервиса до N реплик вы увидели, что:
- сначала рост отношения записанных значений к истекшим
- Redis блокирует операции записи

Как вы думаете, в чем может быть проблема?
 
```
Ответ:

скорее всего не хватка оперативной памяти, мы превысили лимит памяти "maxmemory".

Дополняю:
Возможнo вся память занята истекшими ключами, но еще не удаленными. Redis заблокировался (ACTIVE_EXPIRE_CYCLE_LOOKUPS_PER_LOOP),
чтобы вывести из DB удаленные ключи и снизить их количество менее 25%. Т.к. Redis - однопоточное приложение, то все операции блокируются,
пока он не выполнит очистку. Имеет смысл поиграть со значением hz в redis.conf

```

## Задача 3

Вы подняли базу данных MySQL для использования в гис-системе. При росте количества записей, в таблицах базы,
пользователи начали жаловаться на ошибки вида:
```python
InterfaceError: (InterfaceError) 2013: Lost connection to MySQL server during query u'SELECT..... '
```

Как вы думаете, почему это начало происходить и как локализовать проблему?

Какие пути решения данной проблемы вы можете предложить?

```
Ответ:

Как вы думаете, почему это начало происходить и как локализовать проблему?
Описание задачи намекает на причину: таблица разрослась, в таком случае запросы могут выполняться долго. В сообщении упоминается `SELECT`, возможно в таблице просто не хватает индексов. 

Чтобы найти такие запросы, можно попробовать включить [slow_query_log](https://dev.mysql.com/_doc_/refman/8.0/en/server-system-variables.html#sysvar_slow_query_log).

В документации MySQL ошибке посвящена [эта](https://dev.mysql.com/doc/refman/8.0/en/error-lost-connection.html) статья, в ней перечислены три возможные причины:
* Слишком объёмные запросы на миллионы строк, и рекомендуют увеличить параметр `net_read_timeout`
* Небольшое значение параметра `connect_timeout`, клиент не успевает установить соединение
* Размер сообщения/запроса превышает размер буфера, заданного в переменной ` max_allowed_packet` [на сервере](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_max_allowed_packet) или опцией `--max_allowed_packet` [клиента](https://dev.mysql.com/doc/refman/8.0/en/packet-too-large.html) 

Ещё можно предположить, что проблема в неактивности клиента. Это строга из лога SQLAlchemy, на [StackOverflow](https://stackoverflow.com/questions/29755228/sqlalchemy-mysql-lost-connection-to-mysql-server-during-query) предложили уменьшить параметр `pool_recycle` в SQLAlchemy и увеличить `wait_timeout` в [настройках](https://dev.mysql.com/doc/refman/5.6/en/server-system-variables.html#sysvar_wait_timeout) сервера MySQL.


Какие пути решения данной проблемы вы можете предложить?
Поправить все перечисленные параметры:
* Увеличить на сервере MySQL `wait_timeout`, `max_allowed_packet`, `net_write_timeout` и `net_read_timeout`
* В SQLAlchemy уменьшить `pool_recycle`, сделать меньше `wait_timeout`

Если ошибка пропадёт, возвращать по одному в исходное состояние, так должно получиться локализовать проблему.

Возможно достаточно будет оптимизировать запросы. Их можно найти включив `slow_query_log` и [настроив](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_long_query_time) параметр `long_query_time` на значение близкое к `net_write_timeout` и `net_read_timeout`. 

Если проблема происходит только на селектах, вероятно изменения таймаутов и оптимизации запросов будет достаточно. Если ещё на инсертах - возможно, наоборот, нужно удалить часть индексов. 

```


## Задача 4


Вы решили перевести гис-систему из задачи 3 на PostgreSQL, так как прочитали в документации, что эта СУБД работает с 
большим объемом данных лучше, чем MySQL.

После запуска пользователи начали жаловаться, что СУБД время от времени становится недоступной. В dmesg вы видите, что:

`postmaster invoked oom-killer`

Как вы думаете, что происходит?
```
Ответ:

PostgreSQL не ъхватает памяти
```


Как бы вы решили данную проблему?
```
Ответ:
Percona [предлагают](https://www.percona.com/blog/2019/08/02/out-of-memory-killer-or-savior/) настроить "перевыделение" памяти в `sysctl.conf`:
* `vm.overcommit_memory = 2`
* `vm.overcommit_ratio` без рекомендации, по-умолчанию 60%; если вся БД помещается в ОЗУ, на Хабре [советуют](https://habr.com/ru/company/southbridge/blog/464245/) поставить = 1

Та же можно настроить swap: указать в параметре `vm.swappiness` значение побольше (по-умолчанию = 60), тобы ядро начало использовать своп раньше.

Дополняю:
Postgres недостаточно памяти.
Когда у сервера/процесса заканчивается память, Linux предлагает два пути решения: обрушить систему или завершить процесс, который съедает память.
Out-Of-Memory Killer — это процесс, который завершает приложение, чтобы спасти ядро от сбоя.

По возможности добавить ресурсов (RAM), провести ревизию и отключить/перенести ненужные приложения.
Произвести настройку параметров, затрагивающих память в Postgres:
max_connections
shared_buffer
work_mem
effective_cache_size
maintenance_work_mem

```
---

### Как cдавать задание

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
