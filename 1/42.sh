#!/usr/bin/env bash

while ((1==1))
do
	curl https://localhost:80
	if (($? != 0))
	then
		date >> curl.log
	else exit
	fi
	sleep 5
done
