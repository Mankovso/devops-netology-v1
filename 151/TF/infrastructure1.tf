/*
data — позволяет запрашивать данные. В данном примере мы обращаемся к ресурсу yandex_compute_image с целью поиска идентификатора образа, с которого должна загрузиться наша машина. 
yandex_compute_image — ресурс образа. Его название определено провайдером.
ubuntu_image — произвольное название ресурса. Его мы определили сами и будем использовать в нашем сценарии.
ubuntu-2004-lts — в нашем примере мы запрашиваем данные ресурса, который ищем по family с названием ubuntu-2004-lts. Данное название можно посмотреть в контроль панели хостинга — нужно выбрать образ и кликнуть по нему. Откроется страница с дополнительной информацией. В данном случае ubuntu-2004-lts соответствуем Ubuntu 20.04 LTS.
resource — позволяет создавать различные ресурсы. 
yandex_compute_instance — ресурс виртуальной машины. Его название определено провайдером.
vm-test1 — наше название ресурса для виртуальной машины.
yandex_vpc_network — ресурс сети, определенный провайдером.
network_terraform — наше название для ресурса сети.
yandex_vpc_subnet — ресурс подсети, определенный провайдером.
subnet_terraform — наше название для ресурса подсети.
metadata — обратите особое внимание на передачу метеданных. В данном примере мы передаем содержимое файла, а сам файл рассмотрим ниже.
** в нашем примере будет создана виртуальная машина с названием test1, 2 CPU, 2 Gb RAM в сети 192.168.10.0/24 на базе Ubuntu 20.04 LTS.
*** обратите внимание, что мы указали идентификатор той подсети для виртуальной машины, которую создали также с помощью нашего сценария terraform.
*/


#Создаем машину нат-инст
data "yandex_compute_image" "ubuntu_image" {
  family = "nat-instance-ubuntu"
}

resource "yandex_compute_instance" "instance1" {
  name = "nat-inst"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    ip_address = "192.168.10.254"
    nat       = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
    #ssh-keys = "vagrant:${file("~/.ssh/id_rsa.pub")}"
  }

}

resource "yandex_vpc_network" "network_terraform" {
  name = "net_terraform"
}

resource "yandex_vpc_subnet" "subnet_terraform" {
  name           = "public"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_terraform.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}


#создаем машину с интернетом в той же подсети

data "yandex_compute_image" "ubuntu_internet" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "instance2" {
  name = "ubuntu-internet"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_internet.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    ip_address = "192.168.10.253"
    nat       = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
    #ssh-keys = "vagrant:${file("~/.ssh/id_rsa.pub")}"
  }
}

#Выводим внешний ip в консоль
output "external_ip_address_instance2" {
  value = yandex_compute_instance.instance2.network_interface.0.nat_ip_address
}


#Создаем приватную подсеть

resource "yandex_vpc_subnet" "subnet_private" {
  name           = "private"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_terraform.id
  v4_cidr_blocks = ["192.168.20.0/24"]
  route_table_id = yandex_vpc_route_table.lab-rt.id
}

resource "yandex_vpc_route_table" "lab-rt" {
  name       = "secondtable"
  network_id = yandex_vpc_network.network_terraform.id
  
  static_route {
  destination_prefix = "0.0.0.0/0"
  next_hop_address   = "192.168.10.254"
  }
}

data "yandex_compute_image" "ubuntu_privat" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "instance3" {
  name = "ubuntu-privat"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_privat.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_private.id
    ip_address = "192.168.20.199"
    nat       = false
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
    #ssh-keys = "vagrant:${file("~/.ssh/id_rsa.pub")}"
  }
}
