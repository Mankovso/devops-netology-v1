resource "local_file" "foo" {
    content     = join(" ", 
      ["declare -a IPS=("],
      [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address],
      [yandex_compute_instance.vm-2.network_interface.0.nat_ip_address],
      [yandex_compute_instance.vm-3.network_interface.0.nat_ip_address], 
      [")"]
    )
    filename = "kubespray_declare.sh"
}


resource "local_file" "forKubespray" {
content =  <<-DOC
all:
  hosts:
    node1:
      ansible_host: ${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}
      ansible_user: yc-user
    node2:
      ansible_host: ${yandex_compute_instance.vm-2.network_interface.0.nat_ip_address}
      ansible_user: yc-user
    node3:
      ansible_host: ${yandex_compute_instance.vm-3.network_interface.0.nat_ip_address}
      ansible_user: yc-user
  children:
    kube_control_plane:
      hosts:
        node1:
        node2:
    kube_node:
      hosts:
        node1:
        node2:
        node3:
    etcd:
      hosts:
        node1:
        node2:
        node3:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}


DOC
    filename = "kubespray_hosts"
}
