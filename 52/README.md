# Домашнее задание к занятию "5.2. Применение принципов IaaC в работе с виртуальными машинами"

## Как сдавать задания

Обязательными к выполнению являются задачи без указания звездочки. Их выполнение необходимо для получения зачета и диплома о профессиональной переподготовке.

Задачи со звездочкой (*) являются дополнительными задачами и/или задачами повышенной сложности. Они не являются обязательными к выполнению, но помогут вам глубже понять тему.

Домашнее задание выполните в файле readme.md в github репозитории. В личном кабинете отправьте на проверку ссылку на .md-файл в вашем репозитории.

Любые вопросы по решению задач задавайте в чате учебной группы.

---

## Задача 1

- Опишите своими словами основные преимущества применения на практике IaaC паттернов.
- Какой из принципов IaaC является основополагающим?

Ответ
- Паттерны удобно использовать при автоматическом развертывании, настройке и удалении групп виртуальных машин. Используя паттерны мы всегда видим четкую настройку каждой ВМ, также можем получить опыт если этот паттерн настраивал более опытный человек, основной принцип "идемпотентность", позволяет нам раз за разом разворачивать точные копии ВМ, что имеет огромное значение при разработке.

## Задача 2

- Чем Ansible выгодно отличается от других систем управление конфигурациями?
- Какой, на ваш взгляд, метод работы систем конфигурации более надёжный push или pull?

Ответ
- Ansible простой и мощный инструмент. Он использует для своей работы SSH что позволяет применить его в любой IT сфере не добавляя никаких инструментов обмена данными (PKI). Также он использует метод Push, что снимает с нас необходимость установки агентов (в агентах тоже может быть проблема), а также позволяет нам применять его сразу к свежей ОС.

## Задача 3

Установить на личный компьютер:

- VirtualBox
- Vagrant
- Ansible

*Приложить вывод команд установленных версий каждой из программ, оформленный в markdown.*

Ответ
- вывод команд в моей ОС:
```
smankov@smankov-VirtualBox:~$ ansible --version
ansible 2.9.6
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/smankov/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 3.8.10 (default, Nov 26 2021, 20:14:08) [GCC 9.3.0]
smankov@smankov-VirtualBox:~$ vboxmanage --version
6.1.32r149290
smankov@smankov-VirtualBox:~$ vagrant --version
Vagrant 2.2.19
smankov@smankov-VirtualBox:~$ vagrant box list
bento/ubuntu-20.04 (virtualbox, 202112.19.0)
```
## Задача 4 (*)

Воспроизвести практическую часть лекции самостоятельно.

- Создать виртуальную машину.
- Зайти внутрь ВМ, убедиться, что Docker установлен с помощью команды
```
docker ps
```
Ответ
- запустил склонированный проект (я его скачал как zip т.к. при клонировании совсем не понятное выдает, там совершенно не то что на гитхаб)

```
vagrant@vagrant-ub:~/52/vagrant$ vagrant ssh
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.4.0-91-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 System information disabled due to load higher than 1.0


This system is built by the Bento project by Chef Software
More information can be found at https://github.com/chef/bento
Last login: Sat Apr 16 19:17:07 2022 from 10.0.2.2
vagrant@server1:~$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

```
