# Домашнее задание к занятию "12.4 Развертывание кластера на собственных серверах, лекция 2"
Новые проекты пошли стабильным потоком. Каждый проект требует себе несколько кластеров: под тесты и продуктив. Делать все руками — не вариант, поэтому стоит автоматизировать подготовку новых кластеров.

## Задание 1: Подготовить инвентарь kubespray
Новые тестовые кластеры требуют типичных простых настроек. Нужно подготовить инвентарь и проверить его работу. Требования к инвентарю:
* подготовка работы кластера из 5 нод: 1 мастер и 4 рабочие ноды;
* в качестве CRI — containerd;
* запуск etcd производить на мастере.

<p align="center">
<img src="./img/12.4.png">
</p>

Для установки была использованна команда ansible-playbook -i inventory/mycluster/hosts.yaml cluster.yml -b -v файл hosts.yaml был заранее софрмирован (declare -a IPS=(65.108.242.55 65.21.104.190 65.108.246.97 65.109.134.132 65.108.58.72))

<p align="center">
<img src="./img/12.4_1.png">
</p>

## Задание 2 (*): подготовить и проверить инвентарь для кластера в AWS
Часть новых проектов хотят запускать на мощностях AWS. Требования похожи:
* разворачивать 5 нод: 1 мастер и 4 рабочие ноды;
* работать должны на минимально допустимых EC2 — t3.small.


Все машины были созданы в облаке (Hetzner.com):
<p align="center">
<img src="./img/12.4_dop.png">
</p>

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.
