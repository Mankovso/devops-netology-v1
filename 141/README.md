# Домашнее задание к занятию "14.1 Создание и использование секретов"

## Задача 1: Работа с секретами через утилиту kubectl в установленном minikube

Выполните приведённые ниже команды в консоли, получите вывод команд. Сохраните
задачу 1 как справочный материал.

### Как создать секрет?

```
openssl genrsa -out cert.key 4096
openssl req -x509 -new -key cert.key -days 3650 -out cert.crt \
-subj '/C=RU/ST=Moscow/L=Moscow/CN=server.local'
kubectl create secret tls domain-cert --cert=certs/cert.crt --key=certs/cert.key
```
<p align="center">
<img src="./img/14.1_1.png">
</p>

### Как просмотреть список секретов?

```
kubectl get secrets
kubectl get secret
```
<p align="center">
<img src="./img/14.1_2.png">
</p>

### Как просмотреть секрет?

```
kubectl get secret domain-cert
kubectl describe secret domain-cert
```

<p align="center">
<img src="./img/14.1_3.png">
</p>

### Как получить информацию в формате YAML и/или JSON?

```
kubectl get secret domain-cert -o yaml
kubectl get secret domain-cert -o json
```

<p align="center">
<img src="./img/14.1_4_yaml.png">
</p>
<p align="center">
<img src="./img/14.1_4_json.png">
</p>

### Как выгрузить секрет и сохранить его в файл?

```
kubectl get secrets -o json > secrets.json
kubectl get secret domain-cert -o yaml > domain-cert.yml
```

<p align="center">
<img src="./img/14.1_5.png">
</p>

### Как удалить секрет?

```
kubectl delete secret domain-cert
```

<p align="center">
<img src="./img/14.1_6.png">
</p>

### Как загрузить секрет из файла?

```
kubectl apply -f domain-cert.yml
```

<p align="center">
<img src="./img/14.1_7.png">
</p>

## Задача 2 (*): Работа с секретами внутри модуля

Выберите любимый образ контейнера, подключите секреты и проверьте их доступность
как в виде переменных окружения, так и в виде примонтированного тома.

---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

В качестве решения прикрепите к ДЗ конфиг файлы для деплоя. Прикрепите скриншоты вывода команды kubectl со списком запущенных объектов каждого типа (deployments, pods, secrets) или скриншот из самого Kubernetes, что сервисы подняты и работают, а также вывод из CLI.

---