terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "mankov-terraform-backet"
    region     = "ru-central1"
    key = "diplom_infrastructure.tfstate"
    access_key = ""
    secret_key = ""

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token = ""
  cloud_id = ""
  folder_id = ""
  zone      = "ru-central1-a"
}
