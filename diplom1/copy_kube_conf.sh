#!/bin/bash
#передавать ip контролноды для копирования конфига с удаленного сервера на локальную машину
#пример запуска скрипта: "./copy_kube-conf.sh 51.250.7.118"

if [ -n "$1" ]
then

    ssh yc-user@$1 'mkdir -p $HOME/.kube'
    ssh yc-user@$1 'sudo cp -i /etc/kubernetes/admin.conf .kube/config'
    ssh yc-user@$1 'sudo chown $(id -u):$(id -g) $HOME/.kube/config'
    scp yc-user@$1:/home/yc-user/.kube/config ~/.kube/config
    
    sed -i "s/127.0.0.1/$1/" /home/vagrant/.kube/config
else
	echo 'Нет данных на входе скрипта ' $0
fi