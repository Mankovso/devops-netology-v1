#!/usr/bin/env python3

import os

bash_command = ["cd /c/git_projects/devops-netology", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
for result in result_os.split('\n'):
    if result.find('изменено') != -1:
        prepare_result = result.replace('\tизменено:   ', '')
        print(os.path.abspath(prepare_result))
