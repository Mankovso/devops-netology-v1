# Домашнее задание к занятию "12.5 Сетевые решения CNI"
После работы с Flannel появилась необходимость обеспечить безопасность для приложения. Для этого лучше всего подойдет Calico.
## Задание 1: установить в кластер CNI плагин Calico
Для проверки других сетевых решений стоит поставить отличный от Flannel плагин — например, Calico. Требования: 
* установка производится через ansible/kubespray;
* после применения следует настроить политику доступа к hello-world извне. Инструкции [kubernetes.io](https://kubernetes.io/docs/concepts/services-networking/network-policies/), [Calico](https://docs.projectcalico.org/about/about-network-policy)

В списке подов видим, что установлен калико
<p align="center">
<img src="./img/12.5_1_calico.png">
</p>

Устанавливаем поды и применяем политики из манифестов:
<p align="center">
<img src="./img/12.5_1_np_po.png">
</p>

проверяем сетевую доступность между подами в соответствии со схемой:
<p align="center">
<img src="./img/network-policy.png">
</p>

<p align="center">
<img src="./img/12.5_1_curl_po.png">
</p>

## Задание 2: изучить, что запущено по умолчанию
Самый простой способ — проверить командой calicoctl get <type>. Для проверки стоит получить список нод, ipPool и profile.
Требования: 
* установить утилиту calicoctl;
* получить 3 вышеописанных типа в консоли.

Установка Calico:

```
curl -L https://github.com/projectcalico/calico/releases/download/v3.24.5/calicoctl-linux-amd64 -o calicoctl

chmod +x calicoctl

sudo mv calicoctl /usr/local/bin/
```

переменные окружения:
```
export KUBECONFIG=/root/.kube/config
export DATASTORE_TYPE=kubernetes
```

<p align="center">
<img src="./img/12.5_2.png">
</p>


### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.